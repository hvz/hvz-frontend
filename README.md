<img src="https://img.shields.io/badge/Angular-DD0031?style=for-the-badge&logo=angular&logoColor=white" style="width:100px; height:24px;  border-radius: 4px" />
<img src="https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white" style="width:100px; height:24px; border-radius: 4px" />
<img src="https://img.shields.io/badge/Leaflet-199900?style=for-the-badge&logo=Leaflet&logoColor=white" style="width:100px; height:24px; border-radius: 4px" />
<img src="https://img.shields.io/badge/License-MIT-yellow.svg" style="width:100px; height:24px; border-radius: 4px" />

<div id="top"></div>

<!-- PROJECT LOGO -->
<br />
<div align="center">
    <img src="src/assets/images/zombie.png" alt="Logo" width="83" height="60">

<h1 align="center">Human vs. Zombies</h1>

  <p align="center">
    This repository is part of the Humans vs. Zombies case project application.
    It contains the front-end Angular application that interacts with our back-end Api.
  </p>
</div>

<!-- TABLE OF CONTENTS -->

  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li><a href="#functionality">Functionality</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#authors">Authors</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>



<!-- ABOUT THE PROJECT -->
## About The Project

<img src="about.png" alt="Logo">

This is our final case project for the Noroff Accelerate Full-Stack Java Bootcamp.
We were tasked with creating a front-end, back-end and database for a live geolocation Humans vs. Zombies application similar to games like Pokemon-Go.

The project is deployed through GitLab CI/CD using a Docker image and then hosted on Heroku. Try out the live application here: [Human vs. Zombies](https://ng-hvz.herokuapp.com/)
<p align="center">(<a href="#top">back to top</a>)</p>

## Built With

* [Typescript](https://www.typescriptlang.org/)

* [Angular](https://angular.io/)

* [Angular Material](https://material.angular.io/)

* [Leaflet](https://leafletjs.com/)

* [Heroku](https://www.heroku.com/home)

* [Docker](https://www.docker.com/)

* [Nginx](https://www.nginx.com/)

<p align="right"> (<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

```sh
# Clone the repo
$ git clone git-url
# Cd into the project directory
$ cd project-directory
# Install all dependencies
$ npm install
# Serve the application and open the browser
$ ng serve -o
```

<!-- Functionality EXAMPLES -->
## Functionality

<p>Visitor:</p>
<ul>
<li>Can view the game lobby.</li>
<li>Can Login or Register an account via Auth0</li>
</ul>

<p>User:</p>
<ul>
<li>Can view games in more detail.</li>
<li>Can join and participate in games.</li>
</ul>

<p>Admin:</p>
<ul>
<li>Can create and edit games.</li>
<li>Can manage games and players.</li>
</ul>
<p align="right">(<a href="#top">back to top</a>)</p>

<!-- ROADMAP -->






<!-- CONTRIBUTING -->






<!-- LICENSE -->
## License
Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>

## Authors

*[HvZ Group](https://gitlab.com/hvz)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- Maintainers -->
## Maintainers
<p>[@AxlBrancoDuarte](https://gitlab.com/AxlBrancoDuarte)</p>
<p>[@beau-c](https://gitlab.com/beau-c)</p>
<p>[@g.m.j.w.hendriks](https://gitlab.com/g.m.j.w.hendriks)</p>
<p>[@mr-choi](https://gitlab.com/mr-choi)</p>

<p align="right">(<a href="#top">back to top</a>)</p>


<!-- ACKNOWLEDGMENTS -->
