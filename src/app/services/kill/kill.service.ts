import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { NewKill } from 'src/app/models/kill/new-kill';
import { HttpClient } from "@angular/common/http";
import { API_URL } from 'src/app/constants/app.const';
import { KillDetails } from 'src/app/models/kill/kill-details';

@Injectable({
  providedIn: 'root'
})
export class KillService {

  constructor(private http: HttpClient) { }

  // Returns the kills of a game
  getKills(gameId: number): Observable<KillDetails[]> {
    return this.http.get<KillDetails[]>(`${API_URL}/game/${gameId}/kill`);
  }

  // Returns the kill of a game
  getKillById(gameId: number, killId: number): Observable<KillDetails> {
    return this.http.get<KillDetails>(`${API_URL}/game/${gameId}/kill/${killId}`);
  }

  // Register a new kill
  registerKill(newKill: NewKill, gameId: number) {
    return this.http.post<NewKill>(`${API_URL}/game/${gameId}/kill`, newKill);
  }

}
