import { Injectable } from '@angular/core';
import { GameListItem } from "../../models/game/game-list-item";
import { Observable } from "rxjs";
import { GameDetails } from "../../models/game/game-details";
import { NewGame } from "../../models/game/new-game";
import { ModifyGame } from "../../models/game/modify-game";
import { API_URL } from "../../constants/app.const";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class GameService {

  constructor(private http: HttpClient) { }

  // Returns the game with the given id.
  public getGameDetails(gameId: number): Observable<GameDetails> {
    return this.http.get<GameDetails>(`${API_URL}/game/${gameId}`);
  }

  // Returns the list of games.
  public getGameList(): Observable<GameListItem[]> {
    return this.http.get<GameDetails[]>(`${API_URL}/game`);
  }

  // Creates a new game and returns the created game.
  public createGame(newGame: NewGame): Observable<GameDetails> {
    return this.http.post<GameDetails>(`${API_URL}/game`, newGame);
  }

  // Updates a game and returns the updated game.
  public updateGame(gameId: number, modifyGame: ModifyGame): Observable<GameDetails> {
    return this.http.put<GameDetails>(`${API_URL}/game/${gameId}`, modifyGame);
  }

  // Deletes a game.
  public deleteGame(gameId: number): Observable<void> {
    return this.http.delete<void>(`${API_URL}/game/${gameId}`);
  }
}
