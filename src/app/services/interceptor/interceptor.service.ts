import { Injectable } from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {iif, Observable, switchMap} from "rxjs";
import { IdToken } from "@auth0/auth0-angular";
import {UserService} from "../user/user.service";

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor(private userService: UserService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.userService.isAuthenticated().pipe(
      switchMap((isAuthenticated: boolean) =>
        iif(() => isAuthenticated,
          this.userService.getIdTokenClaims(),
          // Don't request access token if user is not authenticated
          new Observable<undefined>(subscriber => {
            subscriber.next(undefined);
          })
        )
      ),
      switchMap((idToken: IdToken | undefined) => {
        // If the user is not authenticated, we don't need to add the access token to the request.
        if (!idToken) {
          return next.handle(req);
        }

        const headers = req.headers.set('Authorization', `Bearer ${idToken.__raw}`);
        const newRequest = req.clone({headers});
        return next.handle(newRequest);
      }
    ));
  }

}
