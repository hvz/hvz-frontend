import { Injectable } from '@angular/core';
import { LocationCoordinates } from 'src/app/models/location/location-coordinates';

@Injectable({
  providedIn: 'root'
})
export class MapService {

  private _playerLocation?: LocationCoordinates;

  constructor() { }

  // Returns the player location
  get playerLocation() {
    if (!this._playerLocation) {
      throw new Error("No initialized map.")
    }
    return this._playerLocation;
  }

  // Stores the player location
  set playerLocation(location: LocationCoordinates) {
    this._playerLocation = location;
    console.log("Set playerlocation", this._playerLocation)
  }
}
