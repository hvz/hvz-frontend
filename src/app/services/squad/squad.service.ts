import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {SquadDetails} from "../../models/squad/squad-details";
import {HttpClient} from "@angular/common/http";
import {API_URL} from "../../constants/app.const";
import {ModifySquad} from "../../models/squad/modify-squad";
import {SquadListItem} from "../../models/squad/squad-list-item";
import {NewSquad} from "../../models/squad/new-squad";
import {NewSquadCheckin} from "../../models/squadcheckin/new-squad-checkin";
import {SquadCheckinInfo} from "../../models/squadcheckin/squad-checkin-info";

@Injectable({
  providedIn: 'root'
})
export class SquadService {

  constructor(private http:HttpClient) { }

  // Create a new squad
  public createSquad(gameId:number, newSquad:NewSquad) : Observable<SquadDetails> {
    return this.http.post<SquadDetails>(`${API_URL}/game/${gameId}/squad`, newSquad) as Observable<SquadDetails>;
  }

  // Get a list of all squads for a game
  public getSquadList(gameId:number) : Observable<SquadListItem[]> {
    return this.http.get<SquadListItem[]>(`${API_URL}/game/${gameId}/squad`) as Observable<SquadListItem[]>;
  }

  // Join a squad
  public joinSquad(gameId:number, squadId:number) : Observable<SquadDetails> {
    return this.http.post<SquadDetails>(`${API_URL}/game/${gameId}/squad/${squadId}/join`, null) as Observable<SquadDetails>;
  }

  // Leave a squad
  public leaveSquad(gameId:number, squadId:number) : Observable<void> {
    return this.http.delete<void>(`${API_URL}/game/${gameId}/squad/${squadId}/leave`) as Observable<void>;
  }

  // Get squad by id
  public getSquad(gameId:number, squadId:number) : Observable<SquadDetails> {
    return this.http.get<SquadDetails>(`${API_URL}/game/${gameId}/squad/${squadId}`) as Observable<SquadDetails>;
  }

  // Update squad
  public updateSquad(gameId:number, squadId:number, modifySquad:ModifySquad) : Observable<SquadDetails> {
    return this.http.put<SquadDetails>(`${API_URL}/game/${gameId}/squad/${squadId}`, modifySquad) as Observable<SquadDetails>;
  }

  // Delete squad by id
  public deleteSquad(gameId:number, squadId:number) : Observable<void> {
    return this.http.delete<void>(`${API_URL}/game/${gameId}/squad/${squadId}`) as Observable<void>;
  }

  // Checkin to a squad
  public checkinSquad(gameId:number, squadId:number, newSquadCheckin:NewSquadCheckin) : Observable<void> {
    return this.http.post<void>(`${API_URL}/game/${gameId}/squad/${squadId}/checkin`, newSquadCheckin) as Observable<void>;
  }

  // Get squad checkin info
  public getSquadCheckinList(gameId:number, squadId:number) : Observable<SquadCheckinInfo[]> {
    return this.http.get<SquadCheckinInfo[]>(`${API_URL}/game/${gameId}/squad/${squadId}/checkin`) as Observable<SquadCheckinInfo[]>;
  }
}
