import { Injectable } from '@angular/core';
import { AuthService, IdToken, User } from "@auth0/auth0-angular";
import { map, Observable } from "rxjs";
import { LOCAL_TOKEN_KEY, LOCAL_USER_KEY, TOKEN_NAMESPACE_ROLES } from "../../constants/app.const";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private auth0: AuthService) { }

  // Returns the current user, or undefined if there is no user
  public getUser(): Observable<User | null | undefined> {

    const user: string | null = localStorage.getItem(LOCAL_USER_KEY);
    if (user) {
      console.log("UserService: Local user detected. ✅ Skipping auth0 call.");
      // If a user is logged in, return the user object
      return new Observable<User | null | undefined>(observer => {
        observer.next(JSON.parse(user) as User);
      });
    }
    // If no user is logged in, return the auth0 user observable
    console.log("UserService: No local user detected. ⌛ Making auth0 call.");
    return this.auth0.user$.pipe(
      map(user => {
        if (user) {
          console.log("UserService: Storing user in local storage: " + JSON.stringify(user));
          // After login, store the user in local storage
          localStorage.setItem(LOCAL_USER_KEY, JSON.stringify(user));
        }
        return user;
      }
      ));
  }

  // Returns the user's id token
  public getIdTokenClaims(): Observable<IdToken | undefined> {
    const token: string | null = localStorage.getItem(LOCAL_TOKEN_KEY);

    if (token) {

      return new Observable<IdToken | undefined>(observer => {
        const idToken = JSON.parse(token)! as IdToken;

        if (idToken.exp! * 1000 < Date.now()) {
          console.log("UserService: Token has expired. Refreshing token.");
          this.logout("/");
          observer.next(undefined);
          observer.complete()
        }

        observer.next(JSON.parse(token) as IdToken);
      });
    }

    return this.auth0.getIdTokenClaims().pipe(
      map(claims => {
        if (claims) {
          console.log("UserService: Storing id token claims in local storage.");
          if (claims.exp) {
            claims.exp = new Date(claims.exp + (60 * 60 * 24 * 2)).getTime();
          }
          localStorage.setItem(LOCAL_TOKEN_KEY, JSON.stringify(claims));
        }
        return claims;
      }));
  }

  // Redirects the user to the login page
  public login(): void {
    // Make sure there is no user or token in local storage before we log in
    localStorage.removeItem(LOCAL_USER_KEY);
    localStorage.removeItem(LOCAL_TOKEN_KEY);
    this.auth0.loginWithRedirect();
  }

  // Logs the user out of the application
  public logout(returnUri: string): void {
    localStorage.removeItem(LOCAL_USER_KEY);
    localStorage.removeItem(LOCAL_TOKEN_KEY);
    this.auth0.logout({
      returnTo: returnUri
    });
  }

  public isAuthenticated(): Observable<boolean> {

    // If there is a user in local storage, assume they are authenticated
    const user: string | null = localStorage.getItem(LOCAL_USER_KEY);
    if (user) {
      return new Observable<boolean>(observer => {
        observer.next(true);
      });
    }

    return this.auth0.isAuthenticated$;
  }

  // Returns whether the user has the admin role
  public isAdmin(): Observable<boolean> {
    return this.getUser().pipe(
      map(user => {
        return user && user[TOKEN_NAMESPACE_ROLES].includes("ADMIN");
      }
      ));
  }
}
