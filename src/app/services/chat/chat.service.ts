import { Injectable } from '@angular/core';
import { NewChatMessage } from "../../models/chat/new-chat-message";
import { ChatMessageInfo } from "../../models/chat/chat-message-info";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { API_URL } from "../../constants/app.const";

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(private http: HttpClient) { }

  // Send a new chat message
  // Returns the message that was created
  sendMessage(gameId: number, newChatMessage: NewChatMessage): Observable<ChatMessageInfo> {
    return this.http.post<ChatMessageInfo>(`${API_URL}/game/${gameId}/chat`, newChatMessage);
  }

  // Returns chat messages for the given game
  getMessages(gameId: number): Observable<ChatMessageInfo[]> {

    return this.http.get<ChatMessageInfo[]>(`${API_URL}/game/${gameId}/chat`);
    // return new Observable<ChatMessageInfo[]>(subscriber => {
    //   subscriber.next(
    //     [
    //       {
    //         id: 1,
    //         message: "Hello guys Where are you?",
    //         faction: Faction.GLOBAL,
    //         timestamp: new Date(2022, 1, 1, 12, 34, 23),
    //         isAdmin: false,
    //         username: "Axl"
    //       },
    //       {
    //         id: 2,
    //         message: "I bet you would like to know.",
    //         faction: Faction.GLOBAL,
    //         timestamp: new Date(2022, 1, 1, 12, 37, 45),
    //         isAdmin: true,
    //         username: "Beau"
    //       },
    //       {
    //         id: 3,
    //         message: "Looking for a way out.",
    //         faction: Faction.GLOBAL,
    //         timestamp: new Date(2022, 1, 1, 12, 42, 2),
    //         isAdmin: false,
    //         username: "Gea"
    //       },
    //       {
    //         id: 4,
    //         message: "Help! I think I see a zombie! 😱",
    //         faction: Faction.GLOBAL,
    //         timestamp: new Date(2022, 1, 1, 13, 0, 59),
    //         isAdmin: false,
    //         username: "Kevin"
    //       }
    //     ]
    //   )
    // });
  }
}
