import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_URL } from 'src/app/constants/app.const';
import { MissionListItem } from 'src/app/models/mission/mission-list-item';
import { NewMission } from 'src/app/models/mission/new-mission';

@Injectable({
  providedIn: 'root'
})
export class MissionService {

  constructor(private http: HttpClient) { }

  // Get all missions for a game
  getMissions(gameId: number): Observable<MissionListItem[]> {
    return this.http.get<MissionListItem[]>(`${API_URL}/game/${gameId}/mission`);
  }

  // Create a new mission
  registerMission(newMission: NewMission, gameId: number) {
    return this.http.post<NewMission>(`${API_URL}/game/${gameId}/mission`, newMission);
  }
}


// export interface MissionListItem {
//   id: number;
//   title: string;
//   createdAt: Date;
//   marker: LocationCoordinates;
// }
