import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_URL } from 'src/app/constants/app.const';
import { ModifyPlayer } from 'src/app/models/player/modify-player';
import { PlayerDetails } from 'src/app/models/player/player-details';
import { PlayerListItem } from 'src/app/models/player/player-list-item';


@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  constructor(private http: HttpClient) { }

  // Create a new player
  create(gameId: number): Observable<PlayerDetails> {
    return this.http.post<PlayerDetails>(`${API_URL}/game/${gameId}/player`, null);
  }

  // Get a player by id
  get(gameId: number, playerId: number): Observable<PlayerDetails> {
    return this.http.get<PlayerDetails>(`${API_URL}/game/${gameId}/player/${playerId}`)
  }

  // Get all players in a game
  getPlayerList(gameId: number): Observable<PlayerListItem[]> {
    return this.http.get<PlayerListItem[]>(`${API_URL}/game/${gameId}/player`);
  }

  // Remove a player from a game (doesn't actually delete the player itself)
  deletePlayer(playerId: number, gameId: number): Observable<void> {
    return this.http.delete<void>(`${API_URL}/game/${gameId}/player/${playerId}`);
  }

  // Update a player
  updatePlayerState(playerId: number, gameId: number, modifiedPlayer: ModifyPlayer): Observable<any> {
    return this.http.put<any>(`${API_URL}/game/${gameId}/player/${playerId}`, modifiedPlayer)
  }
}
