import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { User } from '@auth0/auth0-angular';
import { BiteDialogComponent } from 'src/app/components/game/bite-dialog/bite-dialog.component';
import { TOKEN_NAMESPACE_ROLES } from "../../constants/app.const"
import { MatDialog, } from "@angular/material/dialog";
import { PlayerService } from 'src/app/services/player/player.service';
import { InfoDialogComponent } from 'src/app/components/game/info-dialog/info-dialog.component';
import { GameService } from 'src/app/services/game/game.service';
import { GameDetails } from 'src/app/models/game/game-details';
import { PlayerDetails } from 'src/app/models/player/player-details';

import { PlayerListItem } from 'src/app/models/player/player-list-item';
import { GameState } from "../../enums/game-state.enum";
import { UserService } from "../../services/user/user.service";
import { ChatMessageInfo } from "../../models/chat/chat-message-info";
import { ChatService } from "../../services/chat/chat.service";

import { LeafletMapComponent } from 'src/app/components/game/leaflet-map/leaflet-map.component';
import { KillService } from 'src/app/services/kill/kill.service';
import { KillDetails } from 'src/app/models/kill/kill-details';
import { KillListItem } from 'src/app/models/kill/kill-list-item';

import { MissionService } from 'src/app/services/mission/mission.service';
import { MissionListItem } from 'src/app/models/mission/mission-list-item';
import { MissionCreatedEvent} from 'src/app/infrastructure/leaflet-map';
import { NewMissionDialogComponent } from 'src/app/components/game/admin-dialogs/new-mission-dialog/new-mission-dialog.component';
import { SquadDetails } from "../../models/squad/squad-details";
import { SquadService } from "../../services/squad/squad.service";
import {SquadCheckinInfo} from "../../models/squadcheckin/squad-checkin-info";
import {SquadTabComponent} from "../../components/game/squad-tab/squad-tab.component";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-game',
  templateUrl: './game.page.html',
  styleUrls: ['./game.page.css']
})
export class GamePage implements OnInit, OnDestroy {

  public readonly SECONDS_BETWEEN_UPDATES: number = 5;

  isSquadShown: boolean = false;
  isChatShown: boolean = false;
  isPlayersShown: boolean = false;

  public readonly enumGameState = GameState;

  private id: number | undefined = undefined;
  private _doneLoading: boolean = false;
  private _hasLoadedChat: boolean = false;

  public isAdmin = false;
  public _user: User | null | undefined;

  public hasJoinedGame: boolean = false;
  public currentPlayer?: PlayerDetails;
  public playerList?: PlayerListItem[];
  public gameDetails?: GameDetails;
  public currentSquad: SquadDetails | undefined = undefined;
  public currentSquadCheckins: SquadCheckinInfo[] = [];

  public chatMessages: ChatMessageInfo[] = [];

  public get gameId(): number | undefined {
    return this.id;
  }

  public killList: KillDetails[] = [];
  public missionList: MissionListItem[] = [];
  public userFound: boolean = false;

  @ViewChild(LeafletMapComponent) private _leafletComponent!: LeafletMapComponent;
  @ViewChild(SquadTabComponent) private _squadTabComponent: SquadTabComponent | undefined;

  public get hasLoadedGame(): boolean {
    return this._doneLoading;
  };

  private timeoutRef: ReturnType<typeof setInterval> | null = null;

  constructor(private route: ActivatedRoute,
    public userService: UserService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private playerService: PlayerService,
    private gameService: GameService,
    private chatService: ChatService,
    private killService: KillService,
    private missionService: MissionService,
    private squadService: SquadService) {
    this.route.params.subscribe(
      params => {
        this.id = params["id"];
      }
    );

    // Obtain user
    userService.getUser().subscribe({
      next: user => {

        if (user) {
          this._user = user;
          this.isAdmin = user[TOKEN_NAMESPACE_ROLES].includes("ADMIN");

          // Check if user has joined game
          // We use the player id 0 to represent the current user
          this.playerService.get(this.gameId as number, 0).subscribe({
            next:
              player => {
                console.log("Player is in the game: " + player.username);
                this.currentPlayer = player;
                this.hasJoinedGame = true;
                this.gameService.getGameDetails(this.id as number).subscribe({
                  next:
                    game => {
                      this.gameDetails = { ...game, published: new Date(game.published.toString().split('[').shift() as string) };
                      this._doneLoading = true;

                      // Start polling for updates after game has loaded
                      this.updateLoop();
                    },
                  error: err => {
                    console.error("Could not get game details: " + err);
                  }
                });
              },
            error: err => {
              // Player hasn't joined, apparently.
              this.hasJoinedGame = false;
              console.log("Player has not joined game yet.");
              this.updateLoop(); // Still update to get the latest info
            }
          });

        }

      }
    });

  }
  ngOnDestroy(): void {
    clearInterval(this.timeoutRef!);
    this.timeoutRef = null;
  }

  ngOnInit(): void {
    this.timeoutRef = setInterval(() => {
      if (this._doneLoading) {
        this.updateLoop()
      }
    }, this.SECONDS_BETWEEN_UPDATES * 1000);
  }

  toggleShowSquad() {
    // Toggle squad visibility
    this.isChatShown = this.isPlayersShown = false;
    this.isSquadShown = !this.isSquadShown;
  }

  toggleShowChat() {
    // Toggle chat visibility
    this.isSquadShown = this.isPlayersShown = false;
    this.isChatShown = !this.isChatShown;
  }

  toggleShowPlayers() {
    // Toggle 'players' visibility
    this.isSquadShown = this.isChatShown = false;
    this.isPlayersShown = !this.isPlayersShown;
  }

  toggleMap() {
    // Toggle map visibility
    this.isChatShown = this.isSquadShown = this.isPlayersShown = false;
  }

  // (Re)loads the data for the game
  public loadGameDetails(): void {
    this.gameService.getGameDetails(this.id as number).subscribe({
      next:
        game => {
          this.gameDetails = { ...game, published: new Date(game.published.toString().split('[').shift() as string) };
          console.log("Loaded the game details.");
          this._doneLoading = true;
        },
      error: err => {
        console.error(err);
      }
    });
  }

  // Loads the player list for the game
  loadPlayerList() {
    this.playerService.getPlayerList(this.id!).subscribe({
      next:
        players => {
          this.playerList = players
          console.log(players)
        },
      error: err => {
        console.error(err);
      }
    });
  }

  // Load any kills that have occurred since the last update
  loadKillList() {
    this.killService.getKills(this.id!).subscribe({
      next:
        kills => {
          const newKills: KillListItem[] = kills.filter(k => {
            return !(this.killList.some(ek => ek.id === k.id));
          });
          this.killList = kills;
          this.registerKillList(newKills);
        },
      error: err => {
        console.error(err);
      }
    });
  }

  // Register new kills in the leaflet map
  registerKillList(newKills: KillListItem[]) {
    for (let kill of newKills) {
      this._leafletComponent.createTombstoneMarkers(kill)
    }
  }

  // (Re)load the list of missions
  loadMissionList() {
    this.missionService.getMissions(this.id!).subscribe({
      next:
        missions => {
          const newMissions: MissionListItem[] = missions.filter(m => {
            return !(this.missionList.some(em => em.id === m.id));
          });
          this.missionList = missions;
          this.registerMissionList(newMissions);
          console.log(missions)
        },
      error: err => {
        console.error(err);
      }
    })
  }

  // Register new missions to the leaflet map
  registerMissionList(newMission: MissionListItem[]) {
    for (let mission of newMission) {
      this._leafletComponent.createMissionMarkers(mission)
    }
  }

  // Loads checkin markers for the current squad
  loadCheckinList(squadId: number):void {
    if (!this.gameId) return;
    this.squadService.getSquadCheckinList(this.gameId, squadId).subscribe({
      next:
        checkins => {
          const newCheckins: SquadCheckinInfo[] = checkins.filter(c => {
            return !(this.currentSquadCheckins.some(ec => ec.id === c.id));
          });
          this.currentSquadCheckins = checkins;
          this.registerCheckinList(newCheckins);
        },
      error: err => {
        console.error(err);
      }
    });
  }

  // Register new checkins to the leaflet map
  private registerCheckinList(newCheckins: SquadCheckinInfo[]): void {
    for (let checkin of newCheckins) {
      console.log("Creating a new leaflet component for checkin " + checkin.id);
      this._leafletComponent.createCheckinMarker(checkin);
    }
  }

  // Clears all checkin markers from the leaflet map and the list of checkins
  public clearCheckinList(): void {
    this.currentSquadCheckins = [];
    this._leafletComponent.clearCheckinMarkers();
  }

  public joinGame(): void {
    if (this.hasJoinedGame)
      return;

    this.playerService.create(this.gameId as number).subscribe({
      next: player => {
        console.log(player.username + " joined the game.");
        this.currentPlayer = player;
        this.hasJoinedGame = true;
        this.loadGameDetails();
      },
      error: err => {
        console.error(err);
      }
    });
  }

  public updateLoop(): void {
    if (this.isAdmin) {
      this.loadPlayerList();
    }
    else {
      // This is a player (potentially) participating in the game
      this.loadCurrentPlayer(); // Refresh the current player
      this.loadCurrentSquad();  // Refresh current squad
    }

    this.loadGameDetails(); // Refresh game details
    this.loadChatMessages(); // Refresh the chat
    this.loadKillList(); // Refresh the list of kills
    this.loadMissionList(); // Refresh the list of missions
  }

  handleMissionCreated = (event: MissionCreatedEvent) => {
    const newMissionLocation = event.location
    this.dialog.open(NewMissionDialogComponent, {
      data: newMissionLocation
    });
  }

  public createNewCheckin() : void {
    if (!this.currentSquad || !this.gameId) return;

    const newCheckinLocation = this._leafletComponent.getPlayerLocation();

    if (!newCheckinLocation) {
      this.snackBar.open("Could not get your current location to check in. Please try again later.", "", {
        duration: 2000,
        horizontalPosition: "left",
        verticalPosition: "bottom",
        panelClass: ["snackbar-error"]
      });
      return;
    }

    const newSquadCheckin = {
      location: newCheckinLocation
    }

    this.squadService.checkinSquad(this.gameId, this.currentSquad.id, newSquadCheckin).subscribe(
      () => {
        // Display a success message
        this.snackBar.open("Good job! You have checked in for your squad.", "", {
          duration: 2000,
          horizontalPosition: "left",
          verticalPosition: "bottom",
          panelClass: ["snackbar-success"]
        });

        // Update the squad list
        if (this._squadTabComponent) {
          this._squadTabComponent.loadSquadList();
        }
        // Also update the current checkin list
        if (this.currentSquad)
          this.loadCheckinList(this.currentSquad.id);
      }
    );
  }

  public loadChatMessages(): void {
    if (!this.gameDetails) return;
    // Load the chat messages
    this.chatService.getMessages(this.gameDetails.id).subscribe({
      next: (messages: ChatMessageInfo[]) => {

        // Ignore updating the chat if there are no new messages
        if (messages.length <= this.chatMessages.length) {
          return;
        }

        // TODO: Separate messages by faction/squad
        this.chatMessages = messages.sort((a, b) => {
          return a.id < b.id ? -1 : 1; // TODO: Sorting by id right now because the timestamps are weird.
        });

        this._hasLoadedChat = true;
      },
      error: (err) => {
        console.error(err);
      }
    });
  }

  public loadCurrentSquad(): void {
    if (!this.gameDetails) return;
    this.squadService.getSquad(this.gameDetails.id, 0).subscribe(
      {
        next: squad => {
          this.currentSquad = squad;

          // Then, load any checkin markers
          this.loadCheckinList(squad.id);
        },
        error: err => {
          this.currentSquad = undefined;
          console.log("Player is not currently in a squad.");
        }
      }
    );
  }

  // Updates the current player
  public loadCurrentPlayer(): void {
    if (!this.gameDetails) return;
    // Load the current player
    this.playerService.get(this.gameDetails.id, 0).subscribe({
      next: (player: PlayerDetails) => {
        this.currentPlayer = player;
      }
    });
  }

  public openBiteCodeDialog(): void {
    const dialogRef = this.dialog.open(BiteDialogComponent, {
      data: this.currentPlayer
    });
  }

  public openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoDialogComponent, {
      height: '400px',
      width: '600px',
      data: this.gameDetails
    });
  }
}
