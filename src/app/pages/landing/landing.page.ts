import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from "@angular/router";
import { MatSnackBar } from "@angular/material/snack-bar";
import { User } from "@auth0/auth0-angular";
import { GameService } from "../../services/game/game.service";
import { TOKEN_NAMESPACE_ROLES } from "../../constants/app.const";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { NewGameDialogComponent } from "../../components/game/admin-dialogs/new-game-dialog/new-game-dialog.component";
import { GameDetails } from "../../models/game/game-details";
import { GameListComponent } from "../../components/landing/game-list/game-list.component";
import { UserService } from "../../services/user/user.service";

@Component({
  selector: 'app-landing',
  templateUrl: './landing.page.html',
  styleUrls: ['./landing.page.css']
})
export class LandingPage implements OnInit {

  @ViewChild('gameList') gameListComponent: GameListComponent | undefined;

  private _isAuthenticated: boolean = false;
  private _isAdmin: boolean = false;
  private _user: User | null | undefined;

  public get isAdmin(): boolean {
    return this._isAdmin;
  }

  public get isAuthenticated(): boolean {
    return this._isAuthenticated;
  }

  public get username(): string | undefined {
    return this._user?.nickname;
  }

  public get role(): boolean | undefined {
    return this._isAdmin;
  }

  constructor(private router: Router,
    private snackBar: MatSnackBar,
    public userService: UserService,
    private gameService: GameService,
    private dialog: MatDialog) {
    this.userService.isAuthenticated().subscribe(
      isAuthenticated => {
        this._isAuthenticated = isAuthenticated;
      }
    );

    userService.getUser().subscribe(
      user => {
        if (user) {
          this._user = user;
          this._isAdmin = user[TOKEN_NAMESPACE_ROLES].includes("ADMIN");
        }
      }
    );
  }

  ngOnInit(): void {
  }

  // Open the dialog to create a new game (admin only)
  public openCreateGameDialog(): void {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "100%";
    dialogConfig.height = "90%";
    if (window.innerWidth <= 600)
      dialogConfig.maxWidth = "100% !important";

    const dialogRef = this.dialog.open(NewGameDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      (createdGame: GameDetails) => {
        if (createdGame) {
          // Display a success message
          this.snackBar.open("Game '" + createdGame.name + "' was successfully created.", "", {
            duration: 2000,
            horizontalPosition: "left",
            verticalPosition: "bottom",
            panelClass: ["snackbar-success"]
          });

          // Also add the new game to the list of games
          if (this.gameListComponent)
            this.gameListComponent.gameList.push(createdGame);
          else
            console.log("Could not add game to list because game list component was not found.");
        }
      }
    );
  }
}
