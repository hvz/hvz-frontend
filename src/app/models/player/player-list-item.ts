/**
 * A single item in a list of players.
 */
export interface PlayerListItem {
  id: number;
  isHuman: boolean;
  username: string;
}
