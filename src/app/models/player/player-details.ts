/**
 * Details about a player that only admins and the player themselves are allowed to see.
 */
export interface PlayerDetails {
  id: number;
  isHuman: boolean;
  isPatientZero: boolean;
  biteCode: string;
  username: string;
}
