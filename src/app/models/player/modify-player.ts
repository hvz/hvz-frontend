/**
 * Player data to be transferred to modify the status of a player (admin only).
 */
export interface ModifyPlayer {
  isHuman: boolean;
  isPatientZero: boolean;
}
