import { LocationCoordinates } from "../location/location-coordinates";

/**
 * Data to be transferred when a new kill needs to be registered.
 */
export interface NewKill {
  biteCode: string;
  killerId?: number;
  victimId?: number;
  location: LocationCoordinates;
}
