import {LocationCoordinates} from "../location/location-coordinates";

/**
 * Details to be displayed when a specific kill is requested.
 */
export interface KillDetails {
  id: number;
  timeOfDeath: Date;
  location: LocationCoordinates;
  killerId: number;
  killerUsername: string;
  victimId: number;
  victimUsername: string;
}
