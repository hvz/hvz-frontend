import {LocationCoordinates} from "../location/location-coordinates";

/**
 * A single kill in a list of kills.
 */
export interface KillListItem {
  id: number;
  timeOfDeath: Date;
  location: LocationCoordinates;
}
