import {LocationCoordinates} from "../location/location-coordinates";

/**
 * Data to be transferred when a kill record needs to be modified.
 * The killer can only change the location.
 */
export interface ModifyKill {
  location: LocationCoordinates;
  killerId: number;
  victimId: number;
}
