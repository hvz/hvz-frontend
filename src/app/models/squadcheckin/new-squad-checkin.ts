import {LocationCoordinates} from "../location/location-coordinates";

/**
 * Data to be transferred when adding a new checkin.
 */
export interface NewSquadCheckin {
  location: LocationCoordinates;
}
