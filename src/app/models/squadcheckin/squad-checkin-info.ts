/**
 * Squad checkin data to be displayed
 */
import {LocationCoordinates} from "../location/location-coordinates";

export interface SquadCheckinInfo {
  id: number;
  timestamp: Date;
  location: LocationCoordinates;
  squadMemberId: number;
  playerId: number;
  username: string;
}
