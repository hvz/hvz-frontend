import {Faction} from "../../enums/faction.enum";

/**
 * Data to be transferred when sending a new chat message.
 */
export interface NewChatMessage {
  message: string;
  faction: Faction;
}
