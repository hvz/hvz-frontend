import {Faction} from "../../enums/faction.enum";

/**
 * Data that every chat message contains.
 */
export interface ChatMessageInfo {
  id: number;
  message: string;
  faction: Faction;
  timestamp: Date;
  username: string;
  isAdmin: boolean;
}
