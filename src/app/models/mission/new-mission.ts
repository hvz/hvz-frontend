import {Faction} from "../../enums/faction.enum";
import {LocationCoordinates} from "../location/location-coordinates";

/**
 * Data to be transferred when creating a new mission (admin only).
 */
export interface NewMission {
  title: string;
  faction: Faction;
  description: string;
  marker: LocationCoordinates;
}
