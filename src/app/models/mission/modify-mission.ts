import {Faction} from "../../enums/faction.enum";
import {LocationCoordinates} from "../location/location-coordinates";

/**
 * Data to be transferred when modifying an existing mission (admin only).
 */
export interface ModifyMission {
  title: string;
  faction: Faction;
  description: string;
  marker: LocationCoordinates;
}
