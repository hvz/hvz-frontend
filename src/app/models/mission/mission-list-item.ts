import {LocationCoordinates} from "../location/location-coordinates";

/**
 * A single item within a list of missions.
 */
export interface MissionListItem {
  id: number;
  title: string;
  createdAt: Date;
  marker: LocationCoordinates;
}
