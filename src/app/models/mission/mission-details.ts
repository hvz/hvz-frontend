import {Faction} from "../../enums/faction.enum";
import {LocationCoordinates} from "../location/location-coordinates";

/**
 * Details to be displayed for a single mission.
 */
export interface MissionDetails {
  id: number;
  title: string;
  faction: Faction;
  createdAt: Date;
  description: string;
  marker: LocationCoordinates;
}
