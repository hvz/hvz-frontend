/**
 * Data to be transferred when adding a new squad
 */
export interface NewSquad {
  name: string;
}
