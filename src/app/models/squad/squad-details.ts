import {SquadMember} from "./squad-member";

/**
 * Represents data to be displayed for a specific squad
 */
export interface SquadDetails {
  id: number;
  name: string;
  squadMembers: SquadMember[];
}
