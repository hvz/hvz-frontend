/**
 * Data to be transferred when modifying an existing squad.
 */
export interface ModifySquad {
  name: string;
}
