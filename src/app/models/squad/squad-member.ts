/**
 * Squad member data for a single member of a squad.
 */
export interface SquadMember {
  joinedAt: Date;
  playerId: number;
  isHuman: boolean
  username: string;
}
