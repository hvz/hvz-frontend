/**
 * Squad data for a single squad in a collection of squads
 */
export interface SquadListItem {
  id: number;
  name: string;
  numberOfMembers: number;
  numberOfDeceased: number;
}
