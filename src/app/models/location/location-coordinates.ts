/**
 * Holds location data, both for display and modification.
 */
export interface LocationCoordinates {
  longitude: number;
  latitude: number;
}
