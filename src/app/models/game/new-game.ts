import { LocationCoordinates } from "../location/location-coordinates";

/**
 * Data to be transferred when creating a new game.
 */
export interface NewGame {
  name: string;
  description: string;
  radius: number;
  center: LocationCoordinates;
}
