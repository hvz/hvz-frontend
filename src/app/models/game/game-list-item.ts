import {GameState} from "../../enums/game-state.enum";

/**
 * Game information to be displayed in a list of games publicly available.
 */
export interface GameListItem {
  id: number;
  name: string;
  gameState: GameState;
  numberOfPlayers: number;
  published: Date;
}
