import { GameState } from "../../enums/game-state.enum";
import {LocationCoordinates} from "../location/location-coordinates";

/**
 * Information about the game to be displayed when selecting a game to see details after login.
 */
export interface GameDetails {
  id: number;
  name: string;
  gameState: GameState;
  numberOfPlayers: number;
  published: Date;
  description: string;
  radius: number;
  center: LocationCoordinates;
}
