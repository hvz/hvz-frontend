import {GameState} from "../../enums/game-state.enum";
import {LocationCoordinates} from "../location/location-coordinates";

/**
 * Data to be transferred when updating a game.
 */
export interface ModifyGame {
  name:string;
  description:string;
  gameState: GameState;
  radius: number;
  center: LocationCoordinates;
}
