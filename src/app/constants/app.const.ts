export const API_URL = "https://hvz-backend-api.herokuapp.com/api/v1";
export const LOCAL_USER_KEY = "HVZ_USER";
export const LOCAL_TOKEN_KEY = "HVZ_TOKEN";
export const TOKEN_NAMESPACE_ROLES = "https://ng-hvz.herokuapp.com/roles";
