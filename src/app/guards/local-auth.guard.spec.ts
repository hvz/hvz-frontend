import { TestBed } from '@angular/core/testing';

import { LocalAuthGuard } from './local-auth-guard.service';

describe('LocalauthGuard', () => {
  let guard: LocalAuthGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(LocalAuthGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
