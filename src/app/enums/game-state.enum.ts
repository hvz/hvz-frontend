// Represents the state of a game
export enum GameState {
  REGISTRATION = "REGISTRATION",
  IN_PROGRESS = "IN_PROGRESS",
  COMPLETED = "COMPLETED"
}
