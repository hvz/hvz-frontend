// Represent the faction of the player
export enum Faction {
  HUMAN = "HUMAN",
  ZOMBIE = "ZOMBIE",
  GLOBAL = "GLOBAL"
}
