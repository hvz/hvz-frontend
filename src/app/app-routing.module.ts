import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'
import { GamePage } from './pages/game/game.page';
import { LandingPage } from './pages/landing/landing.page';
import {LocalAuthGuard} from "./guards/local-auth-guard.service";

const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    component: LandingPage
  },
  {
    path: "lobby",
    component: LandingPage
  },
  {
    path: "game/:id",
    component: GamePage,
    canActivate: [LocalAuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
