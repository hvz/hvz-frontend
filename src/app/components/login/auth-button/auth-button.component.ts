import { Component, OnInit } from '@angular/core';
import {UserService} from "../../../services/user/user.service";

@Component({
  selector: 'app-auth-button',
  templateUrl: './auth-button.component.html',
  styleUrls: ['./auth-button.component.css']
})
export class AuthButtonComponent implements OnInit {

  constructor(private userService:UserService) { }

  private _isAuthenticated: boolean = false;

  public get isAuthenticated(): boolean {
    return this._isAuthenticated;
  }

  public login(): void {
    this.userService.login();
  }

  public logout(): void {
    this.userService.logout(window.location.origin);
  }

  ngOnInit(): void {
    this.userService.isAuthenticated().subscribe({
      next: isAuthenticated => {
        this._isAuthenticated = isAuthenticated;
      }
    });
  }

}
