import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SquadListItem} from "../../../models/squad/squad-list-item";
import {SquadService} from "../../../services/squad/squad.service";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {SquadDetails} from "../../../models/squad/squad-details";
import {NewSquadDialogComponent} from "../new-squad-dialog/new-squad-dialog.component";

@Component({
  selector: 'app-squad-tab',
  templateUrl: './squad-tab.component.html',
  styleUrls: ['./squad-tab.component.css']
})
export class SquadTabComponent implements OnInit {

  public squadList: SquadListItem[] = [];
  @Input() gameId: number = 1;
  @Input() currentSquad: SquadDetails | undefined;
  @Output() public loadCurrentSquad = new EventEmitter<void>();
  @Output() public clearCheckinMarkers = new EventEmitter<void>();
  @Output() public createNewCheckin = new EventEmitter<void>();

  constructor(private squadService:SquadService, private dialog: MatDialog, private snackBar:MatSnackBar) { }

  ngOnInit(): void {
    this.loadSquadList();
  }

  // Loads the list of squads for the game
  public loadSquadList(): void {
    this.squadService.getSquadList(this.gameId).subscribe(
      squadList => {
        this.squadList = squadList;
      }
    );
  }

  public squadListItemClicked(squadListItem: SquadListItem): void {
    console.log(`SquadListItem ${squadListItem.id} clicked`);
  }

  // Joins the given squad for the player
  public joinSquad(squad: SquadListItem): void {
    console.log(`Joining squad ${squad.id}`);
    this.squadService.joinSquad(this.gameId, squad.id).subscribe(
      () => {
        // Display a success message
        this.snackBar.open(`You have joined the '${squad.name}' squad.`, "", {
          duration: 2000,
          horizontalPosition: "left",
          verticalPosition: "bottom",
          panelClass: ["snackbar-success"]
        });

        // Update the squad list
        this.loadSquadList();

        // Also update the current squad
        this.loadCurrentSquad.emit();
      });
  }

  // Opens the dialog to create a new squad
  public openCreateSquadDialog(gameId: number): void {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false
    dialogConfig.autoFocus = true;
    dialogConfig.data = gameId;

    const dialogRef = this.dialog.open(NewSquadDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      (squadDetails: SquadDetails) => {
        if (squadDetails) {
          // Display a success message
          this.snackBar.open("Your squad '" + squadDetails.name + "' was created successfully.", "", {
            duration: 2000,
            horizontalPosition: "left",
            verticalPosition: "bottom",
            panelClass: ["snackbar-success"]
          });

          // Update the squad list
          this.loadSquadList();
        }
      }
    );
  }

  // Called from the squad tab when the user clicks the 'Create New Checkin' button
  public onCheckIn():void {
    this.createNewCheckin.emit();
  }

  public onLeaveSquad(): void {
    if (!this.currentSquad) return;

    console.log(`Leaving squad ${this.currentSquad.id}`);

    this.squadService.leaveSquad(this.gameId, this.currentSquad.id).subscribe(
      () => {
        // Display a success message
        this.snackBar.open("You have left your squad.", "", {
          duration: 2000,
          horizontalPosition: "left",
          verticalPosition: "bottom",
          panelClass: ["snackbar-success"]
        });

        // Update the squad list
        this.loadSquadList();

        // Clear the checkin markers!
        this.clearCheckinMarkers.emit();

        // Also update the current squad
        this.loadCurrentSquad.emit();
      });
  }
}
