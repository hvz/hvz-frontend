import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SquadTabComponent } from './squad-tab.component';

describe('SquadTabComponent', () => {
  let component: SquadTabComponent;
  let fixture: ComponentFixture<SquadTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SquadTabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SquadTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
