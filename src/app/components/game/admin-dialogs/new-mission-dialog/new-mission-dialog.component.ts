import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Faction } from 'src/app/enums/faction.enum';
import { LocationCoordinates } from 'src/app/models/location/location-coordinates';
import { NewMission } from 'src/app/models/mission/new-mission';
import { MissionService } from 'src/app/services/mission/mission.service';

@Component({
  selector: 'app-new-mission',
  templateUrl: './new-mission-dialog.component.html',
  styleUrls: ['./new-mission-dialog.component.css']
})
export class NewMissionDialogComponent implements OnInit {

  public formGroup: FormGroup;
  isProcessingRequest: boolean = false;
  gameId?: number;

  constructor(private missionService: MissionService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private dialogRef: MatDialogRef<NewMissionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public newMissionLocation: LocationCoordinates,) {

    this.formGroup = this.formBuilder.group({
      missionName: [''],
      description: [''],
    });
  }

  ngOnInit(): void {
    this.route.children.forEach(child => {
      child.params.subscribe(params => {
        const id = params['id']
        this.gameId = +id
      })
    })

  }

  create(): void {
    const { description, missionName } = this.formGroup.value;

    const newMission: NewMission = {
      title: missionName,
      faction: Faction.HUMAN,
      description,
      marker: this.newMissionLocation
    }

    this.missionService.registerMission(newMission, this.gameId!).subscribe({
      next: mission => {
        console.log("Mission registered" + mission);
        this.dialogRef.close(mission);
      },
      error: error => {
        console.log("Error creating game: " + error);

      }
    })
  }

  //   if(!this.formGroup.valid) {
  //   return;
  // }

  // const name = this.formGroup.value["name"];
  // const description = this.formGroup.value["description"];

  // console.log(name, description, this.area, this.radius);

  // //TODO: Create a new game using the form data.
  // this._isProcessingRequest = true;
  // try {
  //   this.gameService.createGame({
  //     name,
  //     description,
  //     center: {
  //       latitude: this.area?.lat || 0,
  //       longitude: this.area?.lng || 0
  //     },
  //     radius: this.radius
  //     // TODO: Add location coordinates and area size (radius in km?).
  //   }).subscribe({
  //     next: (game) => {
  //       console.log("Game created: " + game.id);
  //       this._isProcessingRequest = false;
  //       this.dialogRef.close(game);
  //     },
  //     error: (error) => {
  //       console.error("Error creating game: " + error);
  //       this._isProcessingRequest = false;
  //     }
  //   }
  //   );
  // } catch (e) {
  //   console.error(e);
  // }
  //   }

}
