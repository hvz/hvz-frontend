import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from "@angular/material/dialog";
import { FormBuilder, FormGroup } from "@angular/forms";
import { GameService } from "../../../../services/game/game.service";
import { AreaCreatedEvent } from '../../../../infrastructure/leaflet-map';
import { LatLng } from 'leaflet';

@Component({
  selector: 'app-new-game-dialog',
  templateUrl: './new-game-dialog.component.html',
  styleUrls: ['./new-game-dialog.component.css']
})
export class NewGameDialogComponent implements OnInit {

  public formGroup: FormGroup;
  private _isProcessingRequest: boolean = false;

  private area: LatLng | null = null;
  private radius = 0;
  public isAdmin: boolean = true;

  public get isProcessingRequest(): boolean {
    return this._isProcessingRequest;
  }

  constructor(private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<NewGameDialogComponent>,
    private gameService: GameService) {
    this.formGroup = this.formBuilder.group({
      name: [''],
      description: [''],
    });
  }

  ngOnInit(): void {
  }

  handleAreaCreated = (event: AreaCreatedEvent) => {
    this.area = event.area;
    this.radius = event.radius;
  }

  create(): void {

    if (!this.formGroup.valid) {
      return;
    }

    const name = this.formGroup.value["name"];
    const description = this.formGroup.value["description"];

    console.log(name, description, this.area, this.radius);

    //TODO: Create a new game using the form data.
    this._isProcessingRequest = true;
    try {
      this.gameService.createGame({
        name,
        description,
        center: {
          latitude: this.area?.lat || 0,
          longitude: this.area?.lng || 0
        },
        radius: this.radius
        // TODO: Add location coordinates and area size (radius in km?).
      }).subscribe({
        next: (game) => {
          console.log("Game created: " + game.id);
          this._isProcessingRequest = false;
          this.dialogRef.close(game);
        },
        error: (error) => {
          console.error("Error creating game: " + error);
          this._isProcessingRequest = false;
        }
      }
      );
    } catch (e) {
      console.error(e);
    }
  }

  cancel(): void {
    this.dialogRef.close();
  }
}
