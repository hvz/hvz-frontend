import {Component, Inject, OnInit} from '@angular/core';
import {GameService} from "../../../../services/game/game.service";
import {GameDetails} from "../../../../models/game/game-details";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormBuilder, FormGroup} from "@angular/forms";
import {GameState} from "../../../../enums/game-state.enum";
import {ModifyGame} from "../../../../models/game/modify-game";

@Component({
  selector: 'app-update-game-dialog',
  templateUrl: './update-game-dialog.component.html',
  styleUrls: ['./update-game-dialog.component.css']
})
export class UpdateGameDialogComponent implements OnInit {

  public readonly enumGameState = GameState;

  public formGroup: FormGroup;

  private _isProcessingRequest: boolean = false;

  public get isProcessingRequest(): boolean {
    return this._isProcessingRequest;
  }

  constructor(@Inject(MAT_DIALOG_DATA) public game: GameDetails, private gameService: GameService, private formBuilder: FormBuilder, private dialogRef: MatDialogRef<UpdateGameDialogComponent>) {

    console.log(game);

    console.log("Constructor");

    this.formGroup = this.formBuilder.group({
      name: [game.name],
      description: [game.description],
      gameState: [game.gameState.toString()]
    });

    console.log("Constructor end");

  }

  ngOnInit(): void {
    console.log("OnInit");
  }

  public cancel(): void {
    console.log("Cancel");

    this.dialogRef.close();
  }

  public save(): void {
    console.log("Save");
    if (!this.formGroup.valid) {
      return;
    }

    const name = this.formGroup.value["name"];
    const description = this.formGroup.value["description"];

    const modifyGame: ModifyGame = {
      name: name,
      description: description,
      center: this.game.center,
      radius: this.game.radius,
      gameState: this.formGroup.value["gameState"]
    };

    this.gameService.updateGame(this.game.id, modifyGame).subscribe(
      game => {
        this.dialogRef.close(game);
      }
    );
  }
}
