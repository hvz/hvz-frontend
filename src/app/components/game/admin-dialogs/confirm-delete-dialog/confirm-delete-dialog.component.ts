import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {GameListItem} from "../../../../models/game/game-list-item";
import {GameService} from "../../../../services/game/game.service";

@Component({
  selector: 'app-confirm-delete-dialog',
  templateUrl: './confirm-delete-dialog.component.html',
  styleUrls: ['./confirm-delete-dialog.component.css']
})
export class ConfirmDeleteDialogComponent implements OnInit {

  private _isProcessingRequest: boolean = false;

  public get isProcessingRequest(): boolean {
    return this._isProcessingRequest;
  }

  constructor(@Inject(MAT_DIALOG_DATA) public gameListItem:GameListItem,
              private gameService:GameService,
              private dialogRef:MatDialogRef<ConfirmDeleteDialogComponent>) {

  }

  ngOnInit(): void {
  }

  cancel(): void {
    this.dialogRef.close(false);
  }

  confirmDelete(): void {
    this._isProcessingRequest = true;
    this.gameService.deleteGame(this.gameListItem.id).subscribe(
      () => {
        this.dialogRef.close(true);
      }
    );
  }

}
