import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Faction } from '../../../enums/faction.enum'
import { ChatService } from "../../../services/chat/chat.service";
import { ChatMessageInfo } from "../../../models/chat/chat-message-info";
import { NewChatMessage } from "../../../models/chat/new-chat-message";

@Component({
  selector: 'app-chat-tab',
  templateUrl: './chat-tab.component.html',
  styleUrls: ['./chat-tab.component.css']
})
export class ChatTabComponent implements OnInit, AfterViewInit {

  @Input() public gameId!: number;
  @Input() public messages: ChatMessageInfo[] = [];
  @Output() public loadMessages = new EventEmitter<void>();
  @ViewChild('inputFactionChat') private inputFactionChat!: ElementRef;

  public messageBody: string | undefined;

  private _isLoading: boolean = false;

  public get isLoading(): boolean {
    return this._isLoading;
  }

  constructor(private chatService: ChatService) { }

  ngAfterViewInit(): void {
    this.inputFactionChat.nativeElement.focus();
  }

  ngOnInit(): void {
  }

  public getTimeString(timestampString: string): string {
    return new Date(timestampString.split('[').shift() as string).toLocaleTimeString();
  }

  public getDateString(timestampString: string): string {
    return new Date(timestampString.split('[').shift() as string).toLocaleDateString();
  }

  public sendFactionMessage(): void {

    if (!this.messageBody) return; // Don't send an empty message

    const newChatMessage: NewChatMessage = {
      message: this.messageBody,
      faction: Faction.GLOBAL // TODO: For now limiting all messages to the GLOBAL faction (todo make separation)
    }
    this.chatService.sendMessage(this.gameId, newChatMessage).subscribe(
      () => {
        this.messageBody = undefined;
        // Refresh the chat!
        this.loadMessages.emit();
        const chat = document.querySelector(".scrollbar");
        if (chat) chat.scrollTop = chat.scrollHeight - 8;
        this.inputFactionChat.nativeElement.focus();
      }
    )
  }



}
