import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GameDetails } from 'src/app/models/game/game-details';


@Component({
  selector: 'app-info-dialog',
  templateUrl: './info-dialog.component.html',
  styleUrls: ['./info-dialog.component.css']
})
export class InfoDialogComponent implements OnInit {


  constructor(@Inject(MAT_DIALOG_DATA) public game: GameDetails,) {
  }

  ngOnInit(): void {
    console.log(this.game)
  }



}
