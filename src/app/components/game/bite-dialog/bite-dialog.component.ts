import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { NewKill } from 'src/app/models/kill/new-kill';
import { PlayerDetails } from 'src/app/models/player/player-details';
import { KillService } from 'src/app/services/kill/kill.service';
import { MapService } from 'src/app/services/map/map.service';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-bite-dialog',
  templateUrl: './bite-dialog.component.html',
  styleUrls: ['./bite-dialog.component.css']
})
export class BiteDialogComponent implements OnInit {
  biteForm: FormGroup;
  gameId: number = 0;
  private _isProcessingRequest: boolean = false;
  _isAdmin: boolean = false;


  constructor(
    private formbuilder: FormBuilder,
    private killService: KillService,
    private mapService: MapService,
    private route: ActivatedRoute,
    private userService: UserService,
    private dialogRef: MatDialogRef<BiteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public player: PlayerDetails
  ) {
    this.biteForm = this.formbuilder.group({
      biteCode: ['', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(4),
        Validators.pattern("^[abcdefghijklmnpqrstuvwxyz0-9_.-]*$"),
      ]]
    });
  }

  ngOnInit(): void {
    this.route.children.forEach(child => {
      child.params.subscribe(params => {
        const id = params['id']
        this.gameId = +id
      })
    })

    this.userService.isAdmin().subscribe({
      next: isAdmin => {
        this._isAdmin = isAdmin;
      },
      error: err => {
        console.log(err);
      }
    })
  }


  onSubmit(biteForm: FormGroup): void {
    const { biteCode } = biteForm.value;

    if (!biteCode) {
      throw new Error('No biteCode found.');
    }

    const locationCoordinates = this.mapService.playerLocation;

    const kill: NewKill = {
      biteCode: biteCode,
      location: locationCoordinates,
    }
    this._isProcessingRequest = true;

    try {
      this.killService.registerKill(kill, this.gameId).subscribe({
        next: kill => {
          console.log("Kill registered" + kill);
          this._isProcessingRequest = false;
          this.dialogRef.close();
        },
        error: error => {
          console.log("Error creating game: " + error);
          this._isProcessingRequest = false
        }
      })
    } catch (e) {
      console.error(e);
    }

    console.log('game id:', this.gameId)
    console.log('new kill object:', JSON.stringify(kill, null, 2))
  }
}
