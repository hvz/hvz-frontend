import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BiteDialogComponent } from './bite-dialog.component';

describe('BiteDialogComponent', () => {
  let component: BiteDialogComponent;
  let fixture: ComponentFixture<BiteDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BiteDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BiteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
