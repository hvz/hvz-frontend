import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {NewGameDialogComponent} from "../admin-dialogs/new-game-dialog/new-game-dialog.component";
import {SquadService} from "../../../services/squad/squad.service";
import {SquadDetails} from "../../../models/squad/squad-details";
import {NewSquad} from "../../../models/squad/new-squad";

@Component({
  selector: 'app-new-squad-dialog',
  templateUrl: './new-squad-dialog.component.html',
  styleUrls: ['./new-squad-dialog.component.css']
})
export class NewSquadDialogComponent implements OnInit {

  public formGroup: FormGroup;
  private _isProcessingRequest: boolean = false;

  public get isProcessingRequest(): boolean {
    return this._isProcessingRequest;
  }

  constructor(@Inject(MAT_DIALOG_DATA) public gameId: number,
              private formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<NewGameDialogComponent>,
              private squadService:SquadService) {
    this.formGroup = this.formBuilder.group({
      name: ['']
    });
  }

  ngOnInit(): void {
  }

  public create(): void {
    if (!this.formGroup.valid) return;

    this._isProcessingRequest = true;

    const newSquad:NewSquad = {
      name: this.formGroup.value["name"]
    };

    this.squadService.createSquad(this.gameId, newSquad).subscribe(
      (squad:SquadDetails) => {
        this.dialogRef.close(squad);
      }
    );
  }

  public cancel(): void {
    this.dialogRef.close();
  }

}
