import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewSquadDialogComponent } from './new-squad-dialog.component';

describe('NewSquadDialogComponent', () => {
  let component: NewSquadDialogComponent;
  let fixture: ComponentFixture<NewSquadDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewSquadDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewSquadDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
