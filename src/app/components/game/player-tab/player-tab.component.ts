import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ModifyPlayer } from 'src/app/models/player/modify-player';
import { PlayerListItem } from 'src/app/models/player/player-list-item';
import { PlayerService } from 'src/app/services/player/player.service';

@Component({
  selector: 'app-player-tab',
  templateUrl: './player-tab.component.html',
  styleUrls: ['./player-tab.component.css']
})
export class PlayerTabComponent implements OnInit {

  @Input() public playerList?: PlayerListItem[];
  @Output() loadPlayers = new EventEmitter();
  public status: string = "";
  constructor(private playerService: PlayerService) { }
  @Input() public gameId?: number;

  ngOnInit(): void {
    console.log(this.playerList)
  }

  // Toggles the player's human/zombie status
  togglePlayerState(playerId: number): void {
    let modifiedPlayer: ModifyPlayer = {
      "isHuman": false,
      "isPatientZero": false
    }
    this.playerService.updatePlayerState(playerId, this.gameId!, modifiedPlayer)
      .subscribe({
        next: () => {
          this.loadPlayers.emit();
        }
      })
  }

  // Removes a player from the game
  deletePlayer(playerId: number): void {
    this.playerService.deletePlayer(playerId, this.gameId!)
      .subscribe({
        next: () => {
          this.loadPlayers.emit();
        }
      })
  }
}
