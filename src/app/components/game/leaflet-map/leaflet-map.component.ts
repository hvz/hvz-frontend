import { Component, AfterViewInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import {
  CheckinCreatedEvent,
  LeafletMap,
  MissionCreatedEvent,
  PlayerLocatedEvent
} from 'src/app/infrastructure/leaflet-map';

import { GameDetails } from '../../../models/game/game-details';
import { GameState } from 'src/app/enums/game-state.enum';
import { AreaCreatedEvent } from '../../../infrastructure/leaflet-map';
import { MapService } from 'src/app/services/map/map.service';

import { KillListItem } from 'src/app/models/kill/kill-list-item';
import { MissionListItem } from 'src/app/models/mission/mission-list-item';
import {SquadCheckinInfo} from "../../../models/squadcheckin/squad-checkin-info";
import {LocationCoordinates} from "../../../models/location/location-coordinates";



@Component({
  selector: 'app-leaflet-map',
  templateUrl: './leaflet-map.component.html',
  styleUrls: ['./leaflet-map.component.css']
})
export class LeafletMapComponent implements AfterViewInit, OnDestroy {

  @Input() public mapHeight = '100%';
  @Input() public mapWidth = '100%';

  @Input() public fullscreen = false;

  @Output() areaCreated = new EventEmitter<AreaCreatedEvent>();
  @Output() missionCreated = new EventEmitter<MissionCreatedEvent>();
  @Output() checkinCreated = new EventEmitter<CheckinCreatedEvent>();

  @Input() public gameDetails?: GameDetails;
  @Input() public isAdmin = false;
  @Input() public isHuman = true;
  private _map?: LeafletMap;

  constructor(
    private mapService: MapService) {
  }

  ngOnDestroy(): void {
    this._map?.stopTracking()
  }

  ngAfterViewInit(): void {

    // if _game is null, that means we're creating the game there enable createArea.
    // if _game is not null, check game state? we enable mission markers

    const mapElement = document.querySelector('.map-container') as HTMLDivElement;

    mapElement.style.width = this.mapWidth;
    mapElement.style.height = this.mapHeight;

    if (this.fullscreen) {
      mapElement.style.position = 'absolute';
      mapElement.style.top = '0';
      mapElement.style.right = '0';
      mapElement.style.left = '0';
      mapElement.style.bottom = '0';
    }

    // for now, we'll check if we have a valid id param to allow for mission creation
    // we should instead check the game information when that is implemented.
    this._map = new LeafletMap({
      id: 'map',
      isHuman: this.isHuman,
      isAdmin: this.isAdmin,
      createArea: this.isAdmin && !Boolean(this.gameDetails),
      createMissions: this.isAdmin && (this.gameDetails?.gameState === GameState.IN_PROGRESS || this.gameDetails?.gameState === GameState.REGISTRATION),
      onAreaCreated: (event) => this.areaCreated.emit(event),
      onMissionCreated: (event) => this.missionCreated.emit(event),
      onPlayerLocated: (event) => this.setPlayerLocation(event),
      game: this.gameDetails,
    });
  }

  private setPlayerLocation(playerLocatedEvent: PlayerLocatedEvent) {
    if (playerLocatedEvent.location) {
      this.mapService.playerLocation = playerLocatedEvent.location;
    }
  }

  public getPlayerLocation(): LocationCoordinates | null | undefined {
    return this._map?.getPlayerLocation();
  }

  public createTombstoneMarkers(kill: KillListItem): void {
    this._map?.createTombstone(kill)
  }
  public createMissionMarkers(mission: MissionListItem): void {
    this._map?.createMission(mission)
  }
  public createCheckinMarker(squadCheckin: SquadCheckinInfo): void {
    this._map?.createCheckin(squadCheckin);
  }

  public clearCheckinMarkers(): void {
    this._map?.clearCheckinMarkers();
  }

}
