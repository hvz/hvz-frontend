import {Component, OnInit} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Router} from '@angular/router';
import {GameListItem} from 'src/app/models/game/game-list-item';
import {GameService} from 'src/app/services/game/game.service';
import {GameState} from "../../../enums/game-state.enum";
import {UserService} from "../../../services/user/user.service";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {GameDetails} from "../../../models/game/game-details";
import {UpdateGameDialogComponent} from "../../game/admin-dialogs/update-game-dialog/update-game-dialog.component";
import {ConfirmDeleteDialogComponent} from "../../game/admin-dialogs/confirm-delete-dialog/confirm-delete-dialog.component";

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.css']
})
export class GameListComponent implements OnInit {

  public readonly enumGameState = GameState;

  private _isLoading: boolean = false;
  private _isAdmin: boolean = false;
  private _isAuthenticated: boolean = false;

  public get isLoading(): boolean {
    return this._isLoading;
  }

  public get isAdmin(): boolean {
    return this._isAdmin;
  }

  public gameList: GameListItem[] = [];

  constructor(private router: Router,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private gameService: GameService,
    private userService: UserService) {
  }

  ngOnInit(): void {
    this._isLoading = true;

    this.loadGameList();

    this.userService.isAdmin().subscribe({
      next: isAdmin => {
        this._isAdmin = isAdmin;
      },
      error: err => {
        console.error(err);
      }
    });

    this.userService.isAuthenticated().subscribe({
      next: isAuthenticated => {
        this._isAuthenticated = isAuthenticated;
      }
    });
  }

  private loadGameList(): void {
    this.gameService.getGameList().subscribe({
      next: gameList => {
        this.gameList = gameList.sort(
          (a, b) => {
            // Sort by state first
            if (a.gameState === GameState.REGISTRATION && b.gameState !== GameState.REGISTRATION)
              return -1;
            if (b.gameState === GameState.REGISTRATION && a.gameState !== GameState.REGISTRATION)
              return 1;
            if (a.gameState === GameState.IN_PROGRESS && b.gameState !== GameState.IN_PROGRESS)
              return -1;
            if (b.gameState === GameState.IN_PROGRESS && a.gameState !== GameState.IN_PROGRESS)
              return 1;
            // Sort by published date next (the newest games are at the top)
            return a.published > b.published ? 1 : -1;
          }
        );
        this._isLoading = false;
      },
      error: err => {
        console.error(err);
        this.snackBar.open("Error loading game list.", "OK", {
          duration: 3000,
          horizontalPosition: "center",
          verticalPosition: "top"
        });
        this._isLoading = false;
      }
    });
  }

  public openEditGameDialog(gameId: number): void {

    this.gameService.getGameDetails(gameId).subscribe({
        next: gameDetails =>
        {
          const dialogConfig = new MatDialogConfig();

          dialogConfig.disableClose = false
          dialogConfig.autoFocus = true;
          dialogConfig.data = gameDetails;

          const dialogRef = this.dialog.open(UpdateGameDialogComponent, dialogConfig);

          dialogRef.afterClosed().subscribe(
            (updatedGame: GameDetails) => {
              if (updatedGame) {
                // Display a success message
                this.snackBar.open("Game '" + updatedGame.name + "' was successfully updated.", "", {
                  duration: 2000,
                  horizontalPosition: "left",
                  verticalPosition: "bottom",
                  panelClass: ["snackbar-success"]
                });

                // Update the game list
                this.loadGameList();
              }
            }
          );
        }
      }
    );
  }

  public deleteGame(gameListItem: GameListItem): void {
    const dialogConfig:MatDialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = gameListItem;

    const dialogRef = this.dialog.open(ConfirmDeleteDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      (hasDeleted: boolean) => {
        if (hasDeleted) {
          // Display a success message
          this.snackBar.open("Game '" + gameListItem.name + "' was successfully deleted.", "", {
            duration: 2000,
            horizontalPosition: "left",
            verticalPosition: "bottom",
            panelClass: ["snackbar-success"]
          });

          // Update the game list
          this.loadGameList();
        }
      }
    );
  }

  public navigateToGame(gameId: number): void {

    if (!this._isAuthenticated) {
      this.snackBar.open("You must be logged in to view a game.", "OK", {
        duration: 3000,
        horizontalPosition: "center",
        verticalPosition: "bottom",
        panelClass: ["snackbar-error"]
      });
      return;
    }

    this.router.navigateByUrl(`/game/${gameId}`);
  }
}
