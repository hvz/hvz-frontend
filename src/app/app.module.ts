import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { LandingPage } from './pages/landing/landing.page';
import { GamePage } from './pages/game/game.page';
import { LoginPage } from './pages/login/login.page';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthModule } from "@auth0/auth0-angular";
import { AuthButtonComponent } from './components/login/auth-button/auth-button.component';
import { AppRoutingModule } from './app-routing.module';
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatCardModule } from "@angular/material/card";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { MatGridListModule } from "@angular/material/grid-list";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatTabsModule } from "@angular/material/tabs";
import { MatFormFieldModule } from '@angular/material/form-field';
import { ChatTabComponent } from './components/game/chat-tab/chat-tab.component';
import { MatDialogModule } from "@angular/material/dialog";
import { GameListComponent } from './components/landing/game-list/game-list.component';
import { SquadTabComponent } from './components/game/squad-tab/squad-tab.component';
import { NewGameDialogComponent } from "./components/game/admin-dialogs/new-game-dialog/new-game-dialog.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatInputModule } from "@angular/material/input";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { LeafletMapComponent } from './components/game/leaflet-map/leaflet-map.component';
import { BiteDialogComponent } from './components/game/bite-dialog/bite-dialog.component';
import { InfoDialogComponent } from './components/game/info-dialog/info-dialog.component';
import { PlayerTabComponent } from './components/game/player-tab/player-tab.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { InterceptorService } from "./services/interceptor/interceptor.service";
import { UpdateGameDialogComponent } from './components/game/admin-dialogs/update-game-dialog/update-game-dialog.component';
import { MatSelectModule } from "@angular/material/select";
import { ConfirmDeleteDialogComponent } from './components/game/admin-dialogs/confirm-delete-dialog/confirm-delete-dialog.component';
import { NewMissionDialogComponent } from './components/game/admin-dialogs/new-mission-dialog/new-mission-dialog.component';
import { NewSquadDialogComponent } from './components/game/new-squad-dialog/new-squad-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingPage,
    GamePage,
    LoginPage,
    AuthButtonComponent,
    GameListComponent,
    SquadTabComponent,
    ChatTabComponent,
    NewGameDialogComponent,
    LeafletMapComponent,
    BiteDialogComponent,
    InfoDialogComponent,
    PlayerTabComponent,
    UpdateGameDialogComponent,
    ConfirmDeleteDialogComponent,
    NewMissionDialogComponent,
    NewSquadDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AuthModule.forRoot({
      domain: 'hvz-case-project.eu.auth0.com',
      clientId: 'PxTQ034SNa9aKvDJvyb2D9yp6kWdmG2V'
    }),
    MatFormFieldModule,
    MatTabsModule,
    AppRoutingModule,
    MatToolbarModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatGridListModule,
    MatSnackBarModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    FormsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
