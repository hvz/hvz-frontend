import * as L from 'leaflet';
import {Control, FeatureGroup, LatLng, LocationEvent, Map} from 'leaflet';
import 'leaflet-draw';
import {AdminMarker, HumanMarker, MissionMarker, ZombieMarker} from './map-markers';
import {LocationCoordinates} from '../models/location/location-coordinates';
import {GameDetails} from '../models/game/game-details';
import {KillListItem} from '../models/kill/kill-list-item';
import {MissionListItem} from '../models/mission/mission-list-item';
import {SquadCheckinInfo} from "../models/squadcheckin/squad-checkin-info";


const fillColor = 'rgba(219, 48, 39, 0.5)';
const borderColor = 'rgba(219, 48, 39, 0.8)';

const circleOptions: Control.DrawOptions['circle'] = {
    metric: true,
    repeatMode: false,
    shapeOptions: {
        fill: true,
        fillColor,
        color: borderColor,
    },
}

export interface AreaCreatedEvent {
    area: LatLng;
    radius: number;
}
export interface PlayerLocatedEvent {
    location: LocationCoordinates;
}

export interface MissionCreatedEvent {
    location: LocationCoordinates;
}

export interface CheckinCreatedEvent {
  location: LocationCoordinates;
}

interface MapOptions {
    id: string;
    isAdmin: boolean;
    isHuman: boolean;
    createMissions?: boolean;
    createArea?: boolean;
    onAreaCreated?: (event: AreaCreatedEvent) => void;
    onPlayerLocated?: (event: PlayerLocatedEvent) => void;
    onMissionCreated?: (event: MissionCreatedEvent) => void;
    game?: GameDetails;
}

const defaultOptions: MapOptions = {
    id: 'map',
    isAdmin: false,
    isHuman: true,
    createArea: false,
    createMissions: false,
}

const userOptions: L.MapOptions = {
    center: [0, 0],
    dragging: false,
    tap: false,
    doubleClickZoom: false,
    scrollWheelZoom: "center",
    maxZoom: 16
}

export class LeafletMap {
    private readonly _options: MapOptions;

    private readonly _map: Map;
    private readonly _drawnItems: FeatureGroup;
    private readonly _drawControl: L.Control.Draw;

    private _playerLocation?: L.Marker;

    private _gameArea?: L.Circle;

    constructor(
        options?: Partial<MapOptions>,
    ) {
        this._options = { ...defaultOptions, ...options };

        console.log(this._options);

        // TODO: figure out if user is an admin or not and use the `userOptions`.
        const mapOptions = this._options.isAdmin ? { maxZoom: 16 } : userOptions;

        this._map = L.map(this._options.id, mapOptions).fitWorld();

        L.tileLayer('https://stamen-tiles.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.jpg').addTo(this._map)

        this._drawnItems = new L.FeatureGroup();
        this._map.addLayer(this._drawnItems);

        const { createArea, createMissions } = this._options;

        this._drawControl = new L.Control.Draw({
            draw: {
                polygon: false,
                circlemarker: false,
                polyline: false,
                rectangle: false,
                circle: createArea && circleOptions,
                marker: createMissions && {
                    icon: new MissionMarker
                },
            },
            edit: {
                featureGroup: this._drawnItems
            }
        });

        if (this._options.isAdmin) {
            this._map.addControl(this._drawControl);
        }

        this._map.on(L.Draw.Event.CREATED, this.onElementCreated);
        this._map.on(L.Draw.Event.DELETED, this.onElementDeleted);

        this._map.on('locationfound', this.onLocationFound);
        this._map.on('locationerror', this.onLocationError);

        this._map.locate({ setView: true, maxZoom: 0, watch: !this._options.isAdmin });

        if (this._options.game) {
            const { game } = this._options;
            const latlng = L.latLng(game.center.latitude, game.center.longitude)
            this._gameArea = new L.Circle(latlng, {
                ...circleOptions,
                radius: game.radius
            }).addTo(this._map)
        }
    }

    public getTimeString(timestampString: string): string {
        return new Date(timestampString.split('[').shift() as string).toLocaleTimeString();
    }

    public getDateString(timestampString: string): string {
        return new Date(timestampString.split('[').shift() as string).toLocaleDateString();
    }

    onLocationFound = (e: LocationEvent) => {
        if (this._playerLocation) this._playerLocation.remove();

        this._playerLocation = L.marker(e.latlng, { icon: this._options.isAdmin ? new AdminMarker : this._options.isHuman ? new HumanMarker : new ZombieMarker });

        // this.mapService.playerLocation = e.latlng;
        const playerLocation = this.getPlayerLocation();
        if (this._options.onPlayerLocated && playerLocation) {
            this._options.onPlayerLocated({ location: playerLocation });
        }

        this._playerLocation.addTo(this._map)
            .bindPopup(this._options.isAdmin ? "You are viewing/editing the map as an admin." : this._options.isHuman ? "You are still human. Watch out for zombies!" : "You are a zombie. Find and bite humans!", { closeButton: false, keepInView: true, closeOnEscapeKey: false, closeOnClick: false, autoClose: false })
            .openPopup();
    }

    public stopTracking() {
        this._map?.stopLocate();
    }

    onLocationError = (event: L.ErrorEvent) => {
        console.error(event.message);
    }

    onElementCreated = (e: unknown) => {
        const event = e as L.DrawEvents.Created;

        const { layerType: type, layer } = event;

        if (type === 'marker') {
            layer.bindPopup('A Mission!', { closeButton: false, closeOnEscapeKey: true });

            const markerLayer = layer as L.Marker;

            const location = {
                longitude: markerLayer.getLatLng().lng,
                latitude: markerLayer.getLatLng().lat
            }
            console.log(location)

            if (this._options.onMissionCreated) this._options.onMissionCreated({ location })

        }

        if (type === 'circle') {
            this.enableCircleControl(false);

            const circleLayer = layer as L.Circle;

            const area = circleLayer.getLatLng();
            const radius = circleLayer.getRadius();

            if (this._options.onAreaCreated) this._options.onAreaCreated({ area, radius });
        }

        this._drawnItems.addLayer(layer);
    }

    onElementDeleted = (e: unknown) => {
        const event = e as L.DrawEvents.Deleted;

        let deletedArea = false;

        event.layers.eachLayer(layer => {
            if (layer instanceof L.Circle) {
                deletedArea = true;
            }
        });

        if (deletedArea) {
            this.enableCircleControl(deletedArea);
        }
    }

    private enableCircleControl(enabled: boolean) {
        this._drawControl.remove();

        this._drawControl.setDrawingOptions({
            circle: enabled ? circleOptions : false,
        });

        this._map.addControl(this._drawControl);
    }

    public getPlayerLocation(): LocationCoordinates | null {
        const playerLocation = this._playerLocation;
        if (!playerLocation) {
            return null;
        }

        const { lat, lng } = playerLocation.getLatLng();
        return {
            latitude: lat,
            longitude: lng,
          };
      }

    createTombstone(kill: KillListItem) {
        L.marker([kill.location.latitude, kill.location.longitude], {
            icon: L.icon({
                iconUrl: "/assets/images/zombie.png",
                iconSize: L.point(88, 62)
            })
        }).bindPopup(`Time of Kill: ${this.getTimeString(kill.timeOfDeath.toString())} ${this.getDateString(kill.timeOfDeath.toString())} `, { closeButton: false, closeOnEscapeKey: true }).addTo(this._map)
    }

    createMission(mission: MissionListItem) {
        L.marker([mission.marker.latitude, mission.marker.longitude], {
            icon: L.icon({
                iconUrl: "/assets/images/exclamation-mark.png",
                iconSize: L.point(50, 50)
            })
        }).bindPopup(`${mission.title} Mission started at: ${this.getTimeString(mission.createdAt.toString())} ${this.getDateString(mission.createdAt.toString())} `, { closeButton: false, closeOnEscapeKey: true }).addTo(this._map)
    }

    createCheckin(checkin: SquadCheckinInfo) {
      L.marker([checkin.location.latitude, checkin.location.longitude], {
        icon: L.icon({
          iconUrl: "/assets/images/checkin-marker.png",
          iconSize: L.point(50, 80)
        })
      }).bindPopup(`${checkin.username} checked in at: ${this.getTimeString(checkin.timestamp.toString())} ${this.getDateString(checkin.timestamp.toString())} `, { closeButton: false, closeOnEscapeKey: true }).addTo(this._map)
    }

    // Clears the map of all check in markers
    clearCheckinMarkers() {
      this._map.eachLayer(layer => {
        if (layer instanceof L.Marker) {
          if (layer.options.icon?.options.iconUrl === "/assets/images/checkin-marker.png") {
            this._map.removeLayer(layer);
          }
        }
      });
    }

}
