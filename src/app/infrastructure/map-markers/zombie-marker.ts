import * as L from "leaflet";

export const ZombieMarker = L.Icon.extend({
  options: {
    shadowUrl: null,
    iconAnchor: new L.Point(12, 12),
    iconSize: new L.Point(40, 90),
    iconUrl: '/assets/images/zombie-marker.png',
    popupAnchor: [13, -12]
  }
});
