import * as L from "leaflet";

export const AdminMarker = L.Icon.extend({
  options: {
    shadowUrl: null,
    iconAnchor: new L.Point(12, 12),
    iconSize: new L.Point(50, 90),
    iconUrl: '/assets/images/admin-marker.png',
    popupAnchor: [13, -12]
  }
});
