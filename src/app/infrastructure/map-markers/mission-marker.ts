import * as L from "leaflet";

export const MissionMarker = L.Icon.extend({
    options: {
        shadowUrl: null,
        iconSize: new L.Point(50, 50),
        iconUrl: '/assets/images/exclamation-mark.png',


    }
});