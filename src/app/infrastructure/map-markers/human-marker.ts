import * as L from "leaflet";

export const HumanMarker = L.Icon.extend({
    options: {
        shadowUrl: null,
        iconAnchor: new L.Point(12, 12),
        iconSize: new L.Point(40, 60),
        iconUrl: '/assets/images/human-marker.png',
        popupAnchor: [13, -12]
    }
});
