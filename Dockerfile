# Stage 1 - Compile and build codebase
FROM node:16.15.0 as node
WORKDIR /usr/local/app
COPY . .
RUN npm install
RUN npm run build --prod

# Stage 2 - Serve the app with nginx
FROM nginx:alpine
COPY default.conf.template /etc/nginx/templates/default.conf.template
COPY --from=node /usr/local/app/dist/ng-hvz /usr/share/nginx/html
